// The LDR sensor is used in this example

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>
#include <geometry_msgs/Twist.h>


ros::NodeHandle nh;

//the message that is going to be published when the LDR value is changed
std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);

//The LDR value
std_msgs::Int16 LDR_value;
ros::Publisher pub_LDR( "/LDR", &LDR_value);

  //linear and angular values
  geometry_msgs::Twist msg;
  ros::Publisher pub("turtle1/cmd_vel", &msg);


// These constants won't change. They're used to give names to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9; // Analog output pin that the LED is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)


void setup()
{

  pinMode(9, OUTPUT);
  nh.initNode();
  nh.advertise(chatter);
  nh.advertise(pub_LDR);

  nh.advertise(pub);
}

void loop()
{

  // read the analog in value:
  sensorValue = analogRead(analogInPin);

  // map it to the range of the analog out:
  outputValue = map(sensorValue, 400, 900, 0, 5);
  outputValue = map(outputValue, 0, 5, 255, 0);

  //Set the LDR value to the outputValue
  LDR_value.data = outputValue;
  pub_LDR.publish(&LDR_value);

  // change the analog out value:
  analogWrite(analogOutPin, outputValue);

  //let the chatter publishes the correct message
  if (outputValue >= 199) {
    str_msg.data = "It is dark, Full Light";
    //when it is dark, it will publish a full light msg
    chatter.publish( &str_msg );
  }
  else if (outputValue <= 30) {
    str_msg.data = "It is too bright, Light OFF";
    //when it is too bright, it will publish a no light msg
    chatter.publish( &str_msg );

  }
  else {turle
    str_msg.data = "It is not too bright, Half Light";
    //when it is little light, it will publish a half light
    chatter.publish( &str_msg );
  }


  if (outputValue <= 200) {
    msg.linear.x = 0.5;

    pub.publish(&msg);
  } else {
    msg.linear.x = -0.5;

    pub.publish(&msg);
  }


  nh.spinOnce();
  delay(1000);

}

}

