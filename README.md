-# README #

-In this course work 1, we need an LDR, LED, resistance, and the Arduino Nano. 

They are attached as follow to the Arduino Nano board:
1-The LDR to input A0 and to the 5V
2- The LED to input D9 and to the GND
3- The resistance to input A0 and to the GND

The Arduino Code is done to get the LDR value to make the LED turn on when no light, turn off when there is full light, and to half light when little light is there.
In addition, this code will publish a message on ROS to the node to show if the light is on, off, or half light.
Moreover, it will also test the sensor value to check the following:

1- If no light, this means I am blocked and it will publish a Twist message using the Topic name "turtle1/cmd_vel" to the turtlesim to move backward by setting the msg.linear.x value to a minus value such as -0.5
2- If full or half light, this means that nothing is blocking and it will publish a Twist message to the turtlesim using the Topic name "turtle1/cmd_vel" to move forward by setting the msg.linear.x value to a positive value such as 0.5


In ROS Terminal:
I am using the following command in separate Terminal to check my code if it is running correctly:

Terminal 1: $ roscore


Terminal 2: $ rosrun rosserial_arduino serial_node.py _port:=/dev/ttyUSB0


Terminal 3: $ rostopic list
            $ rostopic echo /turtle1/cmd_vel

Terminal 4: $ cd catkin_ws/src
            $ rosrun turtlesim turtlesim_node

Terminal 5: $ rostopic echo /chatter
          or 
 	        $ rostopic echo /LDR



